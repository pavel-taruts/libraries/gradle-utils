package org.taruts.gradleUtils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.taruts.processUtils.ProcessRunner;

import java.io.File;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class GradleBuilder {

    private static final String PROPERTY_LINE_PATTERN_STR = "^(?<name>\\w+):\\s*(?<value>.*)$";
    private static final Pattern PROPERTY_LINE_PATTERN = Pattern.compile(PROPERTY_LINE_PATTERN_STR);

    public static String buildClasses(File directory) {
        return build(directory, "classes");
    }

    public static String build(File directory, String... commandsArg) {

        List<String> commandsList = new ArrayList<>();

        String fileName;
        if (SystemUtils.IS_OS_WINDOWS) {
            fileName = "gradlew.bat";
        } else {
            commandsList.add("sh");
            fileName = "gradlew";
        }

        File gradleWrapperFile = FileUtils.getFile(directory, fileName);
        commandsList.add(gradleWrapperFile.getAbsolutePath());

        commandsList.addAll(Arrays.asList(commandsArg));

        String[] commandsArray = commandsList.toArray(String[]::new);

        return ProcessRunner.runProcess(directory, commandsArray);
    }

    public static Map<String, String> getProperties(File projectDirectory) {
        String resultStr = GradleBuilder.build(projectDirectory, "properties", "--quiet", "--console", "plain");
        return resultStr
                .lines()
                .map(line -> {
                    Matcher matcher = PROPERTY_LINE_PATTERN.matcher(line);
                    if (matcher.find()) {
                        String name = matcher.group("name");
                        String value = matcher.group("value");
                        return new AbstractMap.SimpleImmutableEntry<>(name, value);
                    } else {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
